class CreateEstados < ActiveRecord::Migration[5.1]
  def change
    create_table :estados do |t|
      t.string :nome
      t.string :atualizado_por

      t.timestamps
    end
  end
end
