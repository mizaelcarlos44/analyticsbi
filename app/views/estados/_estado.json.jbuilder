json.extract! estado, :id, :nome, :atualizado_por, :created_at, :updated_at
json.url estado_url(estado, format: :json)
